package com.example.unitconverter;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Spinner mySpinner;
    public void spinnerFunction()
    {
        mySpinner = findViewById(R.id.spinner1);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.MenuItems));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(myAdapter);

    }
        //OnClick function to convert kilograms
    public void convertKilograms(View view)
    {   String size =  mySpinner.getSelectedItem().toString();
        Log.i("SELECTED ITEM: ", size);
        if(size.equals("Kilogram") )
        {
            //assign variable for user input to get this information
            EditText userInput = findViewById(R.id.unitInput);
            String input = userInput.getText().toString();
//                        Try catch statement to catch error of no input
            double guessInt;
            try {
                guessInt = Double.parseDouble(input);
            }
            catch(NumberFormatException ex) {
                //They didn't enter a number.  Pop up a toast or warn them in some other way
                Log.i("Kilogram button clicked", "Input Equals null");
                Toast.makeText(this, "Please input value to be converted", Toast.LENGTH_SHORT).show();
                return;
            }
            //if input has been entered by the user then proceed with the conversions
            if (input != null)
            {
                TextView display1 = findViewById(R.id.display1);
                TextView display2 = findViewById(R.id.display2);
                TextView display3 = findViewById(R.id.display3);
                //Set text to display unit conversion
                display1.setText("Gram ");
                display2.setText("Ounce(Oz)");
                display3.setText("Pound(Lb)");

                Log.i("User Input in Kilogram", input);
                //saving the input from user and converting from string to double data type
                double output = Double.parseDouble(input);
                double result = output * 1000;
                String convert = String.format("%.2f", result);
                double resultOunce = output * 35.274;
                String convertOunce = String.format("%.2f", resultOunce);
                double resultPound = output *2.2046226218;
                String convertPound = String.format("%.2f", resultPound);

                //display the calculated values as a string with 2 decimal places
                TextView output1 = findViewById(R.id.outputUnit1);
                output1.setText(convert);
                TextView output2 = findViewById(R.id.outputUnit2);
                output2.setText(convertOunce);
                TextView output3 = findViewById(R.id.outputUnit3);
                output3.setText(convertPound);
            }
        }
        else
        {
            Toast.makeText(this, "Please select corresponding menu option", Toast.LENGTH_SHORT).show();
        }
   }
        //function to convert celsius
    public void convertCelsius(View view)
    {
        String size =  mySpinner.getSelectedItem().toString();
        Log.i("SELECTED ITEM: ", size);

        if (size.equals("Celsius"))
        {
            //assign variable for user input to get this information
            EditText userInput = findViewById(R.id.unitInput);
            String input = userInput.getText().toString();

            //Try catch statement to catch error of no input
            double guessInt;
            try {
                guessInt = Double.parseDouble(input);
            }
            catch(NumberFormatException ex) {
                //They didn't enter a number.  Pop up a toast or warn them in some other way
                Log.i("Metre button clicked", "Input Equals null");
            Toast.makeText(this, "Please input value to be converted", Toast.LENGTH_SHORT).show();
                return;
            }
            //if input has been entered by the user then proceed with the conversions
            if (input!=null)
            {
                TextView display1 = findViewById(R.id.display1);
                TextView display2 = findViewById(R.id.display2);
                TextView display3 = findViewById(R.id.display3);
                //Set text to display unit conversion
                display1.setText("Fahrenheit ");
                display2.setText("Kelvin");
                display3.setText("");
                Log.i("User Input in Celsius", input);
                //saving the input from user and converting from string to double data type
                double output = Double.parseDouble(input);
                double result = output * 1.8000 + 32;
                String convert = String.format("%.2f", result);
                double resultKelvin = output + 273.15;
                String convertKelvin = String.format("%.2f", resultKelvin);
                //display the calculated values as a string with 2 decimal places
                TextView output1 = findViewById(R.id.outputUnit1);
                output1.setText(convert);
                TextView output2 = findViewById(R.id.outputUnit2);
                output2.setText(convertKelvin);
                TextView output3 = findViewById(R.id.outputUnit3);
                output3.setText("");
            }

        }
        else
        {
            Toast.makeText(this, "Please select corresponding menu option", Toast.LENGTH_SHORT).show();
        }

         }
            //onClick function to convert metres
     public void convertMetres(View view)
    {
         String size =  mySpinner.getSelectedItem().toString();
         Log.i("SELECTED ITEM: ", size);
        if (size.equals("Metre"))
        {
            //assign variable for user input to get this information
            EditText userInput = findViewById(R.id.unitInput);
            String input = userInput.getText().toString();
            //Try catch statement to catch error of no input
            double doubleInput;
            try {
                doubleInput = Double.parseDouble(input);
            }
            catch(NumberFormatException ex) {
                //They didn't enter a number.  Pop up a toast or warn them in some other way
                Log.i("Metre button clicked", "Input Equals null");
                Toast.makeText(this, "Please input value to be converted", Toast.LENGTH_SHORT).show();
                return;
            }
            //if input has been entered by the user then proceed with the conversions
            if (input!=null)
            {
                //Assign variable of TextView Data type through element id
                TextView display1 = findViewById(R.id.display1);
                TextView display2 = findViewById(R.id.display2);
                TextView display3 = findViewById(R.id.display3);
                //Set text to display unit conversion
                display1.setText("Centimetres ");
                display2.setText("Feet ");
                display3.setText("Inches ");

                Log.i("User Input in Metres", input);
                //saving the input from user and converting from string to double data type
                double output = Double.parseDouble(input);
                double result = output * 100;
                String convert = String.format("%.2f", result);
                double resultFeet = output * 3.28084;
                String convertFeet = String.format("%.2f", resultFeet);
                double resultInches = resultFeet * 12.00001;
                //converting into string format and storing with 2 decimal places
                String convertInches = String.format("%.2f", resultInches);
                //display the calculated values as a string with 2 decimal places
                TextView output1 = findViewById(R.id.outputUnit1);
                output1.setText(convert);
                TextView output2 = findViewById(R.id.outputUnit2);
                output2.setText(convertFeet);
                TextView output3 = findViewById(R.id.outputUnit3);
                output3.setText(convertInches);
            }
        }
        else
        {
            Toast.makeText(this, "Please select corresponding menu option", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerFunction();

    }
}
